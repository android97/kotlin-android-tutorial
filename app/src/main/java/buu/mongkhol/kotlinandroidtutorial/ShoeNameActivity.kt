package buu.mongkhol.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ShoeNameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shoe_name)

        val name:String = intent.getStringExtra("name")?:"Unknown"
        val txtName = findViewById<TextView>(R.id.txtName)
        txtName.text = name
    }
}